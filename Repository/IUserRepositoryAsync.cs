using System.Collections.Generic;
using System.Threading.Tasks;

using Authenfication.Models;

namespace Authenfication.Repository
{
    public interface IUserRepositoryAsync
    {
        Task<Response> AddUserAsync(Request request);
        Task<bool> IsUserByRequestExist(LoginRequestViewModel request);

        Task<bool> IsUserByEmailExist(string email);
        Task<bool> IsUserByPhoneExist(string phone);

    }
}