using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

using Authenfication.Context;
using Authenfication.Models;
using Authenfication.Repository;

namespace Authenfication.Repository
{
    public class MySqlUserRepositoryAsync : IDisposable,  IUserRepositoryAsync
    {
        private readonly UserContext _context = null;
        public MySqlUserRepositoryAsync(UserContext context) 
        {
            _context = context;
        }

        public async Task<Response> AddUserAsync(Request request)
        {
            _context.Requests.Add(request);
            var id = await _context.SaveChangesAsync();
            return new Response(){RequestId = id};
        }

        public async Task<bool> IsUserByRequestExist(LoginRequestViewModel request)
        {
                return await (from itemRequest in _context.Requests
                          where itemRequest.SourceId == request.SourceId
                          where itemRequest.SourceSecretKey == request.SourceSecretKey
                          where itemRequest.UserData.Email == request.UserData.Email
                          where itemRequest.UserData.Phone == request.UserData.Phone
                          where itemRequest.UserData.Password == request.UserData.Password
                          select itemRequest).FirstOrDefaultAsync() != null;
        }
       

        public async Task<bool> IsUserByEmailExist(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(b => b.Email.Equals(email)) != null;
            
        }

        public async Task<bool> IsUserByPhoneExist(string phone)
        {
            return await _context.Users.FirstOrDefaultAsync(b => b.Phone.Equals(phone)) != null;
        }

        public void Dispose()
        {

        }
    }
}