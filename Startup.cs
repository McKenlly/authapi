using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Authenfication.Models;
using Authenfication.Repository;
using Authenfication.Context;
using Microsoft.EntityFrameworkCore;
using FluentValidation.AspNetCore;
using Authenfication.Validators;
using FluentValidation;
using Authenfication.Middlewares;
using Authenfication.ActionFilters;

namespace Authenfication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelStateAttribute));
            }).AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddTransient<IValidator<User>, UserValidator>();
            


            var connectionString = Configuration.GetConnectionString("MySqlDatabase");
            services.AddDbContext<UserContext>(options => options.UseMySQL(connectionString));
            // services.AddTransient<IUserRepositoryAsync, MySqlUserRepository>();
            services.AddScoped<IUserRepositoryAsync, MySqlUserRepositoryAsync>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseMiddleware<ExceptionUserMiddleware>();
            app.UseAuthorization();
            // app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
/*{
"request": {
"userData": {
"givenName": "string",
"surname": "string",
"middleName": "string",
"email": "string",
"phone": "string",
"password": "string",
"personalDataAgree": true,
"emailSubscribeAgree": true,
"genderId": "string",
"birthDate": "2020-03-03T06:53:36.452Z"
}
"sourceId": "string",
"sourceSecretKey": "string"
}
}*/