using System;
using System.ComponentModel.DataAnnotations;

namespace Authenfication.Models
{
    public class Warning
    {
        public int Code { get; set; }
        public string FieldName { get; set; }
        public string Message { get; set; }

    }
}