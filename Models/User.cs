using System;

namespace Authenfication.Models
{

    public class User
    {
        public int UserId { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string Email {get; set;}
        public string Phone {get; set;}
        public string Password {get; set;}
        public Boolean PersonalDataAgree {get; set;} = false;
        public Boolean EmailSubscribeAgree {get; set;} = false;
        public string Gender {get; set;}
        public string BirthDate {get; set;}

    }
}