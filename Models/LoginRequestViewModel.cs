using System;
using System.ComponentModel.DataAnnotations;

namespace Authenfication.Models
{
    //AutoMapper
    public class LoginRequestViewModel
    {
        public int RequestId { get; set; }
        public User UserData { get; set; }
        public string SourceId { get; set; }
        public string SourceSecretKey {get; set;}
        
    }
}