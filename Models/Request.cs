using System;
// using Microsoft.EntityFrameworkCore;
// using MySql.Data.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Authenfication.Models
{
    public class Request
    {
        public int RequestId { get; set; }
        public User UserData { get; set; }
        public string SourceId { get; set; }
        public string SourceSecretKey {get; set;}
        
    }
}