using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Results;

namespace Authenfication.Models
{
    public class Response
    {
        public int RequestId { get; set; }
        public User UserData { get; set; }
        // public string SourceId { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        // public string SourceSecretKey {get; set;}
        public int ErrorCode { get; set; }
        public IList<ValidationFailure> Warnings  {get; set;}
        // public IEnumerable<Warning> Warnings2  {get; set;}

    }
}