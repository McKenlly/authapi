using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Authenfication.Models;
using Authenfication.Validators;
using FluentValidation;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace Authenfication.Middlewares
{
    public class ExceptionUserMiddleware
    {
        private const string JsonContentType = "application/json";

        private readonly RequestDelegate _request;

        public ExceptionUserMiddleware(RequestDelegate request)
        {
            _request = request;
        }
        public Task Invoke(HttpContext context) => this.InvokeAsync(context);
        async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _request(context);
            }
            //Not worked
            catch (ValidationException exception)
            {
                var response = new Response() {
                    UserData = null,
                    Success = false,
                    Message = exception.Message,
                    ErrorCode = (int)HttpStatusCode.MultipleChoices,
                    
                };
                context.Response.StatusCode = (int) HttpStatusCode.MultipleChoices;
                context.Response.ContentType = JsonContentType;

                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(response));

                context.Response.Headers.Clear();
            }
            catch (Exception exception) 
            {
                var response = new Response() {
                    UserData = null,
                    Success = false,
                    Message = exception.Message,
                    ErrorCode = (int)HttpStatusCode.MultipleChoices,
                    
                };
                context.Response.StatusCode = (int) HttpStatusCode.MultipleChoices;
                context.Response.ContentType = JsonContentType;

                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(response));

                context.Response.Headers.Clear();
            }
        }
        

    }

    
}