using System;
using Authenfication.Models;
using FluentValidation;
using System.Text.RegularExpressions;

namespace Authenfication.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public static string REGEX_PHONE = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";
        public static string UNCORRECT_PHONE_MESSAGE = "Sorry, your number phone not correct";
        public static string UNCORRECT_BIRTHDATE_MESSAGE = "Sorry, your birthdate not correct";
        public static string UNCORRECT_EMAIL_MESSAGE = "Sorry, your email address not correct";

        public UserValidator()
        {
            RuleFor(m => m.GivenName).NotEmpty().MinimumLength(4).MaximumLength(10);
            RuleFor(m => m.Surname).NotEmpty().MinimumLength(4).MaximumLength(10);
            RuleFor(m => m.MiddleName).MaximumLength(15);
            RuleFor(m => m.Email).NotEmpty().MaximumLength(20);
            RuleFor(m => m.Email).EmailAddress().WithMessage(UNCORRECT_EMAIL_MESSAGE);
            RuleFor(m => m.Phone).Custom((phone, context) => {
                if (!Regex.IsMatch(phone, REGEX_PHONE)) 
                {
                    context.AddFailure(UNCORRECT_PHONE_MESSAGE);
                }
            });
            RuleFor(m => m.Password).NotEmpty().MinimumLength(7).MaximumLength(20);
            RuleFor(m => m.Gender).NotEmpty();

            RuleFor(m => m.BirthDate).NotEmpty();
            RuleFor(m => m.BirthDate).Custom((date, context) => {
                DateTime temp;
                if(!DateTime.TryParse(date, out temp))
                {
                    context.AddFailure(UNCORRECT_BIRTHDATE_MESSAGE);
                }
            });


        }
    }
}