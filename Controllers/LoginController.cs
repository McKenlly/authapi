using System.Threading.Tasks;
using Authenfication.Models;
using Authenfication.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Authenfication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IUserRepositoryAsync _repository;
        
        public LoginController(IUserRepositoryAsync repository)
        {
            _repository = repository;
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] LoginRequestViewModel request)
        {
            return Ok(await _repository.IsUserByRequestExist(request));
        }
    }
}