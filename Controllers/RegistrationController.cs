using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authenfication.Models;
using Authenfication.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Authenfication.Validators;

namespace Authenfication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegistrationController : ControllerBase
    {
        private readonly IUserRepositoryAsync _repository;
        
        public RegistrationController(IUserRepositoryAsync repository)
        {
            _repository = repository;
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Request request)
        {
            //Костыль костылю рознь
            var validator = new UserValidator().Validate(request.UserData);
            if (validator.IsValid) 
            {
                var res = await _repository.AddUserAsync(request);
                res.Success = true;
                res.ErrorCode = 0;
                return Ok(res);
            } else 
            {
                return BadRequest(new Response() {
                    Warnings = validator.Errors,
                    Message = "something wrong",
                    ErrorCode = 300,
                    Success = false
                });
            }
        }
    }
}
