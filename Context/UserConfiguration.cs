using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MySql.Data.EntityFrameworkCore;

using Authenfication.Models;

namespace Authenfication.Context
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
        {
            public void Configure(EntityTypeBuilder<User> builder)
            {
                builder.ToTable("users");
                builder.HasKey(u => u.UserId).HasName("pk_user");
                builder.Property(u => u.GivenName).HasColumnName("name").IsRequired();
                builder.Property(u => u.Surname).HasColumnName("surname").IsRequired();
                builder.HasIndex(u => u.Email).IsUnique();
                builder.HasIndex(u => u.Phone).IsUnique();
                builder.Property(u => u.Password).IsRequired();
                builder.Property(u => u.Gender).IsRequired();
                builder.Property(u => u.BirthDate).HasColumnName("birth_date");
            }
        }
}