using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MySql.Data.EntityFrameworkCore;

using Authenfication.Models;

namespace Authenfication.Context
{
    public class RequestConfiguration : IEntityTypeConfiguration<Request>
        {
            public void Configure(EntityTypeBuilder<Request> builder)
            {
                builder.ToTable("requests");
                builder.HasKey(u => u.RequestId).HasName("pk_request");
                builder.HasIndex(u => u.SourceId).IsUnique();
                builder.Property(u => u.SourceSecretKey).HasColumnName("secret_key").IsRequired();
                builder.HasOne(u => u.UserData);
            }
        }
}